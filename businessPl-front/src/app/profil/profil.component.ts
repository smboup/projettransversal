import { Component, OnInit } from '@angular/core';
import { ToprocessService } from '../services/toprocess.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  entreprises;
  user;
  infos;

  constructor(private toprocessservice: ToprocessService,private router: Router) { }
  
  ngOnInit() {
    this.get();
    this.getUser();
  }
  get(){
    this.toprocessservice.getEntreprise().subscribe((data) => {
      console.log(data);
      this.entreprises = data;
    });
  }

  getUser(){
    this.toprocessservice.getUser().subscribe((data) => {
      console.log(data);
      this.user = data;
      this.infos = data['userprofil'];
    });
  }
  // save(form){
  //   this.toprocessservice.saveEntreprise(form.value).subscribe((data) => {
  //     console.log(data);
  //   });
  // }
  redirect(){
    this.router.navigateByUrl('process/entreprise');
  }
}
