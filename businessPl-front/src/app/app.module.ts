import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule } from '@angular/material';

//importation des modules
import { AppRoutingModule } from './process/app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { ProcessModule } from './process/process.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';


//importation des components
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component'
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { EntrepriseComponent } from './process/entreprise/entreprise.component';
import { FinancementComponent } from './process/financement/financement.component';
import { FooterComponent } from './footer/footer.component';
import { ToprocessService } from './services/toprocess.service';
import { ProfilComponent } from './profil/profil.component';
import { ChargeComponent } from './process/charge/charge.component';
import { DemarrageComponent } from './process/demarrage/demarrage.component';
import { PretbankComponent } from './process/pretbank/pretbank.component';
import { ChiffrannComponent } from './process/chiffrann/chiffrann.component';
import { ChiffrmensComponent } from './process/chiffrmens/chiffrmens.component';
import { DashboardComponent } from './process/dashboard/dashboard.component';
import { ProcessingModule } from './processing/processing.module';
import { RecaputilatifComponent } from './processing/recaputilatif/recaputilatif.component';
import { PlanfinancierComponent } from './processing/planfinancier/planfinancier.component';


const Approutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'profil', component: ProfilComponent},
  {path: 'auth/register', component: RegisterComponent},
  {path: 'auth/login', component: LoginComponent},
  {path: 'login/register', component: RegisterComponent},
  // {path: 'auth/logout', component: LoginComponent},
  {path: 'process/entreprise', component: EntrepriseComponent},
  {path: 'process/finance', component: FinancementComponent},
  {path: 'process/charge', component: ChargeComponent},
  {path: 'process/demarrage', component: DemarrageComponent},
  {path: 'process/pretbank', component: PretbankComponent},
  {path: 'process/chiffrann', component: ChiffrannComponent},
  {path: 'process/chiffrmens', component: ChiffrmensComponent},
  {path: 'dashboard/:id', component: DashboardComponent},
  {path: 'dashboard/:id/recap', component: RecaputilatifComponent},
  {path: 'recap/planfinancier', component: PlanfinancierComponent},

  
  
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ProfilComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    ProcessModule,
    ProcessingModule,
    NgbModule,
    MatTabsModule,
    HttpClientModule,
    RouterModule.forRoot(Approutes)
  ],
  providers: [ToprocessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
