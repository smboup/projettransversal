import { TestBed } from '@angular/core/testing';

import { ToprocessService } from './toprocess.service';

describe('ProcessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToprocessService = TestBed.get(ToprocessService);
    expect(service).toBeTruthy();
  });
});
