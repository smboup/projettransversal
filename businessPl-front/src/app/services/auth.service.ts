import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { User } from '../auth/models/user';
import { JwtResponse } from '../auth/models/jwt-response';

import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

	AUTH_SERVER = "http://localhost:8000";

	authSubject = new BehaviorSubject(false);

	register(user: User): Observable<JwtResponse> {
		return this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/api/authentication/register`, user).pipe(
		  tap((res:  JwtResponse ) => {

		    if (res.user) {
		      localStorage.set("ACCESS_TOKEN", res.token);
		      this.authSubject.next(true);
		    }
		  })

		);
	}
		
	singIn(user: User): Observable<JwtResponse> {
		return this.httpClient.post(`${this.AUTH_SERVER}/api/authentication/login`, user).pipe(
		  tap(async (res: JwtResponse) => {

		    if (res.user) {
		      localStorage.setItem("ACCESS_TOKEN", res.token);
		      this.authSubject.next(true);
		    }
		  })
		);
	}

	signOut() {
		localStorage.removeItem("ACCESS_TOKEN");
		this.authSubject.next(false);
  	}

  	isAuthenticated() {
    	return  this.authSubject.asObservable();
	}
}

