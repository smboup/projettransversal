import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
// import { Http, Headers, Response } from '@angular/http';

// importation des composants
import { Entreprise } from '../process/models/entreprise';
import { JwtResponse1 } from '../process/models/jwt-response1';
import { tap } from 'rxjs/operators';
import {Finance} from '../process/models/finance';
import { JwtResponse6 } from '../process/models/jwt-response6';
import { Demarrage } from '../process/models/demarrage';
import { Chiffrmens } from '../process/models/chiffrmens';
import { Chiffrann } from '../process/models/chiffrann';
import { Charge } from '../process/models/charge';
import { Pretbank } from '../process/models/pretbank';
import { JwtResponse } from '../auth/models/jwt-response';


@Injectable({
  providedIn: 'root'
})
export class ToprocessService {

  constructor(private httpClient: HttpClient) { }

  AUTH_SERVER = 'http://localhost:8000';

  authSubject = new BehaviorSubject(false);

  public ID_entreprise = localStorage.getItem("ID_entreprise");

  saveEntreprise(entreprise: Entreprise): Observable<JwtResponse1> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    const header = new HttpHeaders({'Content-Type': 'application/json', Authorization: 'Token ' + token});
    return this.httpClient.post<JwtResponse1>(`${this.AUTH_SERVER}/api/authentication/process/entreprise/`,  entreprise, { headers : header });
  }

  getEntreprise(): Observable<JwtResponse1> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    const header = new HttpHeaders({'Content-Type': 'application/json', Authorization: 'Token ' + token});
    return this.httpClient.get<JwtResponse1>(`${this.AUTH_SERVER}/api/authentication/process/entreprise`, { headers : header });
  }

  saveFinance(finance: Finance): Observable<JwtResponse6> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    const header = new HttpHeaders({'Content-Type': 'application/json', Authorization: 'Token ' + token});
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/finance/`,  finance, { headers : header });
  }
  getFinance(id: number): Observable<JwtResponse6> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    const header = new HttpHeaders({'Content-Type': 'application/json', Authorization: 'Token ' + token});
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/listfinance/${id}`, { headers : header });

  }

  //A function which save each instance of charge
  saveCharge(charge: Charge): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/charge/`,  charge, { headers : header });
  }
    
  //A function which get each instance of charge
  getCharge(id:number): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/listcharge/${id}`, { headers : header })
  }

  //A function which save each instance of chiffrann
  saveChiffrann(chiffran: Chiffrann): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/chiffrann/`,  chiffran, { headers : header })
  }

  //A function which get each instance of chiffrann
  getChiffran(id:number): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/listchiffr/${id}`, { headers : header });
  }

  //A function which save each instance of chiffrmens
  saveChiffrmens(chiffrmens: Chiffrmens): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/chiffrmens/`,  chiffrmens, { headers : header })
  }

  //A function which get each instance of chiffrann
  getChiffrmens(): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/chiffrmens`, { headers : header })
  }

  //A function which get each instance of pretbank
  savePretbank(Pretbank: Pretbank): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/pret/`,  Pretbank, { headers : header })
  }

  getPretbank(id: number): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/listpret/${id}`, { headers : header })
  }

  //A function which save each instance of Demarrage
  saveDemarrage(demarrages: Demarrage): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.post<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/demarrage/`,  demarrages, { headers : header })
  }

  getDemarrage(id: number): Observable<JwtResponse6>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse6>(`${this.AUTH_SERVER}/api/authentication/process/listdemarrage/${id}`, { headers : header })
  }

  detailEntreprise(id: number):Observable<JwtResponse1>{
    const token = localStorage.getItem('ACCESS_TOKEN')
    const header = new HttpHeaders({"Content-Type":"application/json", Authorization: "Token "+ token})
    return this.httpClient.get<JwtResponse1>(`${this.AUTH_SERVER}/api/authentication/process/entreprise/${id}`, { headers : header })
  }

  getUser(): Observable<JwtResponse> {
		const token = localStorage.getItem('ACCESS_TOKEN');
		const header = new HttpHeaders({'Content-Type': 'application/json', Authorization: 'Token ' + token});
		return this.httpClient.get<JwtResponse>(`${this.AUTH_SERVER}/api/authentication/process/profile`, { headers : header });
	  }


}

