import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-planfinancier',
  templateUrl: './planfinancier.component.html',
  styleUrls: ['./planfinancier.component.css']
})
export class PlanfinancierComponent implements OnInit {

  constructor(private toprocessservice: ToprocessService) { }

  fondroulement;
  rentabilite;
  marge_annee = [];
  chiffre_daffaire2;
  chiffre_daffaire3;
  valeur = [];
  charges;
  finances;
  pretbanks;
  demarrages;
  chiffres;
  achat_cons:any = [];
  Charges:any = [];
  Charge1;
  Charge2;
  Charge3;
  chiffre_daff_ann1;
  result = [];
  dotation;
  remboursement1;
  remboursement2;
  remboursement3;
  autofinance = [];
  Cout_total = [];
  Excedent_Insuffisance = [];

  ngOnInit() {
    //recuperation des donnees pour faire les calculs
    var Finances = localStorage.getItem('allfinances');
    var Pretbanks = localStorage.getItem('allprets');
    var Demarrages = localStorage.getItem('alldemarrages');
    var chiffres = localStorage.getItem('allchiffres');
    this.chiffre_daff_ann1 = localStorage.getItem('allchiffres');
    var pourcentage1 = localStorage.getItem('pourcentage1');
    var pourcentage2 = localStorage.getItem('pourcentage2');
    this.Charge1 =  localStorage.getItem('Charge1');
    this.Charge2 =  localStorage.getItem('Charge2');
    this.Charge3 =  localStorage.getItem('Charge3');
    console.log(this.Charges[0]);
    var pourcentage_charge = localStorage.getItem('pourcentage_charge');
    const amortissement = 3;
  
    const id= localStorage.getItem('ID_entreprise');
    var duree = localStorage.getItem('duree');
    var taux = localStorage.getItem('taux');
    console.log(duree, taux, Pretbanks);
    this.getCharge(id);
    this.getFinancement(id);
    this.getPret(id);
    this.getDemarrage(id);
    this.getChiffrann(id);
    
    //calcul 
    this.calcul_de_fondroulement(Finances, Pretbanks, Demarrages);
    this.verif_rentable(chiffres, this.Charge1, Demarrages, pourcentage_charge, amortissement);
    this.marge_brute(chiffres, pourcentage_charge, pourcentage1, pourcentage2);
    this.valeur_ajoute(this.marge_annee, this.Charge1, this.Charge2, this.Charge3);
    this.achat_consomme(chiffres, this.chiffre_daffaire2, this.chiffre_daffaire3, pourcentage_charge);
    this.Resultat_net_comptable(Demarrages, this.valeur, amortissement);
    this.Dotation(Demarrages, amortissement);
    this.remboursement_banque(Pretbanks,duree, taux);
    this.autofinancement(this.remboursement1, this.remboursement2, this.remboursement3, this.valeur);
    this.cout_total(this.Charge1, this.Charge2, this.Charge3, this.achat_cons);
    this.excedent_insuffisance(this.chiffre_daff_ann1, this.chiffre_daffaire2, this.chiffre_daffaire3, this.Cout_total);
  }

  //Calcul du fond de roulement
  calcul_de_fondroulement(Finances, Pretbanks, Demarrages){
    this.fondroulement = (parseInt(Finances) + parseInt(Pretbanks)) - parseInt(Demarrages)
  };

  //calcul de dotation
  Dotation(demarrage, amortissement){
    this.dotation = Math.round(demarrage/amortissement);
  }
  //verification de la rentabilite du projet sur 1 an
  verif_rentable(chiffre_annuel1 , Charges, Demarrages, pourcentage_charge, amortissement){
    var charge_variable = chiffre_annuel1 * (pourcentage_charge/100)
    var besoin_demarrage = Math.round((Demarrages/amortissement))

    var depense = Charges + charge_variable + besoin_demarrage

    if ((chiffre_annuel1 - depense)>0){
      this.rentabilite = "est rentable";
    }else{
      this.rentabilite = "n'est pas rentable";
    }
  }

  //calcul de la marge brute
  marge_brute(chiffre_annuel1, pourcentage_charge, pourcentage1, pourcentage2){
    var chiffre_daffaire1 =  parseInt(chiffre_annuel1); 
    this.chiffre_daffaire2 = Math.round(chiffre_daffaire1 *(1 + ((pourcentage1)/100)));
    this.chiffre_daffaire3 = Math.round(chiffre_daffaire1 *(1 + ((pourcentage2)/100)));

    var charge_dexploitation1 = Math.round((chiffre_annuel1) * ((pourcentage_charge)/100));
    var charge_dexploitation2 = Math.round((this.chiffre_daffaire2) * ((pourcentage_charge)/100));
    var charge_dexploitation3 = Math.round((this.chiffre_daffaire3) * ((pourcentage_charge)/100));
    
    this.marge_annee[0] = Math.round(chiffre_daffaire1 - charge_dexploitation1);
    this.marge_annee[1] = Math.round((this.chiffre_daffaire2) - charge_dexploitation2);
    this.marge_annee[2] = Math.round((this.chiffre_daffaire3) - charge_dexploitation3);
  }

  //calcul de l'achat consomme
  achat_consomme(chiffre_annuel1, chiffre_daffaire2, chiffre_daffaire3, pourcentage_charge){
    this.achat_cons[0] = Math.round((chiffre_annuel1) * ((pourcentage_charge)/100));
    this.achat_cons[1] = Math.round((chiffre_daffaire2) * ((pourcentage_charge)/100));
    this.achat_cons[2] = Math.round((chiffre_daffaire3) * (parseInt(pourcentage_charge)/100));
  }
  //calcul de la valeur ajoutee
  valeur_ajoute(marge_annee, Charge1, Charge2, Charge3){
    
    this.valeur[0] = parseInt(marge_annee[0]) - parseInt(Charge1)
    this.valeur[1] = parseInt(marge_annee[1]) - parseInt(Charge2)
    this.valeur[2] = parseInt(marge_annee[2]) - parseInt(Charge3)
  }
  //calcul de cout de total
  cout_total(Charge1, Charge2, Charge3, achat_cons){
    this.Cout_total[0] = Math.round(parseInt(Charge1) + parseInt(achat_cons[0]))
    this.Cout_total[1] = Math.round(parseInt(Charge2) + parseInt(achat_cons[1]))
    this.Cout_total[2] = Math.round(parseInt(Charge3) + parseInt(achat_cons[2]))
  }

  //calcul de l'excedent ou l'insuffisance
  excedent_insuffisance(chiffre_daff_ann1, chiffre_daffaire2, chiffre_daffaire3, Cout_total){
    this.Excedent_Insuffisance[0] = parseInt(chiffre_daff_ann1) - parseInt(Cout_total[0]);
    this.Excedent_Insuffisance[1] = parseInt(chiffre_daffaire2) - parseInt(Cout_total[1]);
    this.Excedent_Insuffisance[2] = parseInt(chiffre_daffaire3) - parseInt(Cout_total[2]);
  }
  //calcul du resultat net comptable
  Resultat_net_comptable(demarrage, valeur, amortissement){
    this.result[0] = parseInt(valeur[0]) - Math.round(((demarrage)/(amortissement)));
    this.result[1] = parseInt(valeur[1]) - Math.round(((demarrage)/(amortissement)));
    this.result[2] = parseInt(valeur[2]) - Math.round(((demarrage)/(amortissement)));
  }

  //calcul du remboursement par an
  remboursement_banque(Pret, duree, taux){
    var somme_a_rembourser = Math.round((Pret) * (1 + (parseInt(taux)/100)))
    var somme_mensuelle = Math.round((somme_a_rembourser/(duree)))
      if (parseInt(duree)<=12){
        this.remboursement1 = somme_a_rembourser;
        this.remboursement2 = 0;
        this.remboursement3 = 0;
      }else if(parseInt(duree) >12 && parseInt(duree) <=24){
          var restant_mois = duree - 12;
          this.remboursement1 = (somme_mensuelle * 12);
          this.remboursement2 = (somme_mensuelle * restant_mois);
          this.remboursement3 = 0;
      }else if(parseInt(duree) > 24 && parseInt(duree) <= 36){
        restant_mois = duree - 24;
        this.remboursement1 = (somme_mensuelle * 12);
        this.remboursement2 = (somme_mensuelle * restant_mois);
        this.remboursement3 = 0;
      }else{
          this.remboursement1 = (somme_mensuelle * 12);
          this.remboursement2 = (somme_mensuelle * 12);
          this.remboursement3 = (somme_mensuelle * 12);
      }
    }

  //calcul de l'autofiancement
  autofinancement(remboursement1, remboursement2, remboursement3, valeur){
    this.autofinance[0] = parseInt(valeur[0]) - parseInt(remboursement1);
    this.autofinance[1] = parseInt(valeur[1]) - parseInt(remboursement2);
    this.autofinance[2] = parseInt(valeur[2]) - parseInt(remboursement3);
  }

  //declaration des fonctions pour affaicher les donnees
  getCharge(id){
    this.toprocessservice.getCharge(id).subscribe((data) => {
      var somme: any=0;
      console.log(data);
      this.charges = data['charges'];
    });
  }

  getFinancement(id){
    this.toprocessservice.getFinance(id).subscribe((data) => {
      var somme:any = 0;
      console.log(data);
      this.finances = data['finances'];
    });

  }

  getPret(id){
    this.toprocessservice.getPretbank(id).subscribe((data) => {
      var somme:any = 0;
      console.log(data);
      this.pretbanks = data['prets'];
    });

  }

  getDemarrage(id){
    this.toprocessservice.getDemarrage(id).subscribe((data) => {
      var somme:any = 0;
      console.log(data);
      this.demarrages = data['demarrages'];
    });

  }

  getChiffrann(id){
    this.toprocessservice.getChiffran(id).subscribe((data) => {
      console.log(data);
      this.chiffres = data['chiffres'];
    });
  }

  public downloadPDF(){
    var data = document.getElementById('contentToConvert');
      html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }
}
