import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanfinancierComponent } from './planfinancier.component';

describe('PlanfinancierComponent', () => {
  let component: PlanfinancierComponent;
  let fixture: ComponentFixture<PlanfinancierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanfinancierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanfinancierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
