import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecaputilatifComponent } from './recaputilatif/recaputilatif.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthRoutingModule } from '../auth/auth-routing.module';
import { PlanfinancierComponent } from './planfinancier/planfinancier.component';
import { MatTabsModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [RecaputilatifComponent, PlanfinancierComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    AuthRoutingModule
  ]
})
export class ProcessingModule { }
