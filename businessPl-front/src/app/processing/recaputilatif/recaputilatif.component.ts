import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';



@Component({
  selector: 'app-recaputilatif',
  templateUrl: './recaputilatif.component.html',
  styleUrls: ['./recaputilatif.component.css']
})
export class RecaputilatifComponent implements OnInit {

  constructor(private toprocessservice: ToprocessService) { }
  charges;
  finances;
  pretbanks;
  demarrages;
  chiffres;

  ngOnInit() {
    const id= localStorage.getItem('ID_entreprise');
    this.getCharge(id);
    this.getFinancement(id);
    this.getPret(id);
    this.getDemarrage(id);
    this.getChiffrann(id);
  }

  //declaration des fonctions pour affaicher les donnees
  getCharge(id){
    this.toprocessservice.getCharge(id).subscribe((data) => {
      var somme1: any=0;
      var somme2: any=0;
      var somme3: any=0;
      console.log(data);
      this.charges = data['charges'];
      for(let charge of data['charges']){
        somme1+=parseInt(charge.monant_ann1);
        somme2+=parseInt(charge.monant_ann2);
        somme3+=parseInt(charge.monant_ann3);
      }

      localStorage.setItem('Charge1', somme1);
      localStorage.setItem('Charge2', somme2);
      localStorage.setItem('Charge3', somme3);
    });
  }
  getFinancement(id){
    this.toprocessservice.getFinance(id).subscribe((data) => {
      var somme:any = 0;
      console.log(data);
      this.finances = data['finances'];
      for(let finance of data['finances']){
        somme+=parseInt(finance.montant);
      }
      localStorage.setItem('allfinances', somme);
    });

  }
  getPret(id){
    this.toprocessservice.getPretbank(id).subscribe((data) => {
      var somme:any = 0;
      var duree:any = 0;
      var taux:any = 0;
      console.log(data);
      this.pretbanks = data['prets'];
      for(let pret of data['prets']){
        somme+=parseInt(pret.montant);
        duree+=parseInt(pret.duree);
        taux+=parseInt(pret.taux);
      }
      localStorage.setItem('allprets', somme);
      localStorage.setItem('duree', duree);
      localStorage.setItem('taux', taux);
    });

  }
  getDemarrage(id){
    this.toprocessservice.getDemarrage(id).subscribe((data) => {
      var somme:any = 0;
      console.log(data);
      this.demarrages = data['demarrages'];
      for(let demarrage of data['demarrages']){
        somme+=parseInt(demarrage.monant);
      }
      localStorage.setItem('alldemarrages', somme);
    });

  }
  getChiffrann(id){
    this.toprocessservice.getChiffran(id).subscribe((data) => {
      console.log(data);
      this.chiffres = data['chiffres'];
      var montant:any=0;
      var pourcentage1:any=0;
      var pourcentage2:any=0;
      var pourcentage_charge:any=0;
      for(let chiffre of data['chiffres']){
        montant+=parseInt(chiffre.montant);
        pourcentage1+=parseInt(chiffre.pourcentage1);
        pourcentage2+=parseInt(chiffre.pourcentage2);
        pourcentage_charge+=parseInt(chiffre.pourcentage_charge);
      }
      localStorage.setItem('allchiffres', montant);
      localStorage.setItem('pourcentage1', pourcentage1);
      localStorage.setItem('pourcentage2', pourcentage2);
      localStorage.setItem('pourcentage_charge', pourcentage_charge);
    });
  }

  @ViewChild('content', { static: false }) content: ElementRef;

  public downloadPDF(){
    var data = document.getElementById('contentToConvert');
      html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }
}
