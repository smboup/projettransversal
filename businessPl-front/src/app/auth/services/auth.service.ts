import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';



import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { JwtResponse } from '../models/jwt-response';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

	AUTH_SERVER = "http://localhost:8000";

	authSubject = new BehaviorSubject(false);

	register(user: User): Observable<JwtResponse> {
		return this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/api/authentication/register`, user).pipe(
		  tap((res:  JwtResponse ) => {
		
		    
		    this.authSubject.next(true);
		    
		  })

		);
	}
		
	singIn(user: User): Observable<JwtResponse> {
		return this.httpClient.post(`${this.AUTH_SERVER}/api/authentication/login`, user).pipe(
		  tap(async (res: JwtResponse) => {
			console.log(res)
		    // if (res.user) {
		    //   localStorage.setItem("ACCESS_TOKEN", res.token);
		    //   this.authSubject.next(true);
		    // }
		  })
		);
	}

	signOut() {
		localStorage.removeItem("ACCESS_TOKEN");
		localStorage.removeItem("ID_entreprise");
		localStorage.removeItem("allchiffres");
		localStorage.removeItem("pourcentage1");
		localStorage.removeItem("pourcentage2");
		localStorage.removeItem("pourcentage_charge");
		localStorage.removeItem("alldemarrages");
		localStorage.removeItem("allprets");
		localStorage.removeItem("duree");
		localStorage.removeItem("taux");
		localStorage.removeItem("allfinances");
		localStorage.removeItem("Charge3");
		localStorage.removeItem("Charge2");
		localStorage.removeItem("Charge1");
		this.authSubject.next(false);
  	}

  	isAuthenticated() {
    	return  this.authSubject.asObservable();
	}

}

