import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../models/user';
import { longStackSupport } from 'q';
import { CompileShallowModuleMetadata } from '@angular/compiler';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit() {
  }

  login(form){
  	console.log(form.value);
	  	this.authService.singIn(form.value).subscribe((res)=>{
	      console.log("Logged in!");
	      this.router.navigateByUrl('profil');
	    });    
    }
  
}
