import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit() {
  }

  register(form){
  	// console.log(form.value);
  	this.authService.register(form.value).subscribe((res) => {
      console.log("registered");
      console.log(res)
     
				
			localStorage.setItem("ACCESS_TOKEN", res.token);
		      
		    
      this.router.navigateByUrl('profil');
    });
  }

}
