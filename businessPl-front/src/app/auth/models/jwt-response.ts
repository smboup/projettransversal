import { EmailValidator } from '@angular/forms';

export class JwtResponse {

	user: {
		first_name: string,
		last_name: string,
		username: string,
		email: string,
		password: string,
	}
	token: string;
}
