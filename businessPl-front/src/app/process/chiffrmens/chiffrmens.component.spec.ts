import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiffrmensComponent } from './chiffrmens.component';

describe('ChiffrmensComponent', () => {
  let component: ChiffrmensComponent;
  let fixture: ComponentFixture<ChiffrmensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiffrmensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiffrmensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
