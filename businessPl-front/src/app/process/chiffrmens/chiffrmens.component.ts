import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chiffrmens',
  templateUrl: './chiffrmens.component.html',
  styleUrls: ['./chiffrmens.component.css']
})
export class ChiffrmensComponent implements OnInit {

  constructor(private processService: ToprocessService, private router: Router) { }

  chiffrmens;
  ngOnInit() {
  }

  saveChiffrmens(form){
    console.log(form.value);
    this.processService.saveChiffrmens(form.value).subscribe((res) => {
      this.router.navigateByUrl('');
    });
  }

  get() {
    this.processService.getChiffrmens().subscribe((data) => {
      console.log(data);
      this.chiffrmens = data;
    });
  }
}
