import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToprocessService } from '../../services/toprocess.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-financement',
  templateUrl: './financement.component.html',
  styleUrls: ['./financement.component.css']
})
export class FinancementComponent implements OnInit {

  constructor(private processService: ToprocessService, public activeModal: NgbActiveModal) { }

  finances;
  public ID_entreprise = localStorage.getItem("ID_entreprise");
  ngOnInit() {
  }

  saveFinance(form) {
    console.log(form.value);
    this.processService.saveFinance(form.value).subscribe((res) => {
    });
  }
// recuper des données d'entreprises
  // get() {
  //   this.processService.getFinance().subscribe(data => {
  //     console.log(data);
  //     this.finances = data;
  //   });
  // }
}
