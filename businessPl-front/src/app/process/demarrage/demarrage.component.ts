import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-demarrage',
  templateUrl: './demarrage.component.html',
  styleUrls: ['./demarrage.component.css']
})
export class DemarrageComponent implements OnInit {

  constructor(private processService: ToprocessService, private route: ActivatedRoute, public activeModal: NgbActiveModal) { }

  demarrages;
  public ID_entreprise = localStorage.getItem("ID_entreprise");
  ngOnInit() {
    console.log(this.ID_entreprise);
  }
  saveDemarrage(form){
    console.log(form.value);
    this.processService.saveDemarrage(form.value).subscribe((res) => {

    });
  }

}
