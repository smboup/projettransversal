export interface JwtResponse1 {
    entreprise: {
        id: number;
        intitule_projet: string;
        statut_juridique: string;
        n_tel: number;
        email: string;
        description: string;
        location: string;
        nature: string;
    };

    token: string;
}
