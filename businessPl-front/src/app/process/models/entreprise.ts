export interface Entreprise {
    id: number;
    intitule_projet: string;
    statut_juridique: string;
    n_tel: number;
    email: string;
    location: string;
    nature: string;
}
