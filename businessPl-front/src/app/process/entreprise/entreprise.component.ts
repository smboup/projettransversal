import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToprocessService } from '../../services/toprocess.service';
import { Entreprise } from '../models/entreprise';
import { JwtResponse1 } from '../models/jwt-response1';
@Component({
  selector: 'app-entreprise' ,
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.css']
})
export class EntrepriseComponent implements OnInit {

  constructor(private processService: ToprocessService, private router: Router) { }

  entreprises;

  ngOnInit() {
  }

  enregister(form){
    console.log(form.value);
    this.processService.saveEntreprise(form.value).subscribe((res) => {
      this.router.navigateByUrl('profil');
    });
  }

  get() {
    this.processService.getEntreprise().subscribe((data) => {
      console.log(data);
      this.entreprises = data;
    });
  }
  

}
