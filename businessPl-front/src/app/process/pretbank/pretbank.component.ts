import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pretbank',
  templateUrl: './pretbank.component.html',
  styleUrls: ['./pretbank.component.css']
})
export class PretbankComponent implements OnInit {

  constructor(private processService: ToprocessService, public activeModal: NgbActiveModal) { }

  entreprises;
  public ID_entreprise = localStorage.getItem("ID_entreprise");
  ngOnInit() {
  }

  savepretbank(form){
    console.log(form.value);
    this.processService.savePretbank(form.value).subscribe((res) => {
    });
  }

  // get() {
  //   this.processService.getPretbank().subscribe((data) => {
  //     console.log(data);
  //     this.entreprises = data;
  //   });
  // }

}
