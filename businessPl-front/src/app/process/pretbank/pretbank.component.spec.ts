import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretbankComponent } from './pretbank.component';

describe('PretbankComponent', () => {
  let component: PretbankComponent;
  let fixture: ComponentFixture<PretbankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretbankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretbankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
