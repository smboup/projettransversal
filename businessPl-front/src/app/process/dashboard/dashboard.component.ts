import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DemarrageComponent } from '../demarrage/demarrage.component';
import { ChargeComponent } from '../charge/charge.component';
import { ChiffrannComponent } from '../chiffrann/chiffrann.component';
import { PretbankComponent } from '../pretbank/pretbank.component';
import { FinancementComponent } from '../financement/financement.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private processService: ToprocessService, private route: ActivatedRoute, private router: Router, private modalService: NgbModal) { }

  entreprise;
  ID;
  url;

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    localStorage.setItem("ID_entreprise", id);
    console.log(id);
    this.processService.detailEntreprise(id).subscribe((data) => {
      console.log(data);
      return this.entreprise = data;
    });
  }

  
  open1() {
    const modalRef = this.modalService.open(DemarrageComponent);
  }

  open2() {
    const modalRef = this.modalService.open(ChargeComponent);
  }

  open3() {
    const modalRef = this.modalService.open(FinancementComponent);
  }
  
  open4() {
    const modalRef = this.modalService.open(PretbankComponent);
  }

  open5() {
    const modalRef = this.modalService.open(ChiffrannComponent);
  }

  redirect(url){
    this.router.navigateByUrl(url);
  }

} 
