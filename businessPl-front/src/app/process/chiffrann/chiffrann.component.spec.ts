import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiffrannComponent } from './chiffrann.component';

describe('ChiffrannComponent', () => {
  let component: ChiffrannComponent;
  let fixture: ComponentFixture<ChiffrannComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiffrannComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiffrannComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
