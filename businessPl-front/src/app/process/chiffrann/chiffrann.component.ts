import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-chiffrann',
  templateUrl: './chiffrann.component.html',
  styleUrls: ['./chiffrann.component.css']
})
export class ChiffrannComponent implements OnInit {

  constructor(private processService: ToprocessService, public activeModal: NgbActiveModal) { }

  chiffranns;
  public ID_entreprise = localStorage.getItem("ID_entreprise");
  ngOnInit() {
  }

  saveChiffrann(form){
    console.log(form.value);
    this.processService.saveChiffrann(form.value).subscribe((res) => {
    });
  }

  // get() {
  //   this.processService.getChiffran().subscribe((data) => {
  //     console.log(data);
  //     this.chiffranns = data;
  //   });

}
