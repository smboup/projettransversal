import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { FinancementComponent } from './financement/financement.component';
import { PretbankComponent } from './pretbank/pretbank.component';
import { DemarrageComponent } from './demarrage/demarrage.component';
import { ChargeComponent } from './charge/charge.component';
import { ChiffrannComponent } from './chiffrann/chiffrann.component';
import { ChiffrmensComponent } from './chiffrmens/chiffrmens.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthRoutingModule } from '../auth/auth-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatTabsModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [EntrepriseComponent, FinancementComponent, PretbankComponent, DemarrageComponent, ChargeComponent, ChiffrannComponent, ChiffrmensComponent, DashboardComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    AuthRoutingModule
  ]
})
export class ProcessModule { }
