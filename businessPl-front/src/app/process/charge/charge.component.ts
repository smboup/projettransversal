import { Component, OnInit } from '@angular/core';
import { ToprocessService } from 'src/app/services/toprocess.service';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.css']
})
export class ChargeComponent implements OnInit {

  constructor(private processService: ToprocessService, public activeModal: NgbActiveModal) { }

  charges;
  public ID_entreprise = localStorage.getItem("ID_entreprise");
  ngOnInit() {
  }

  saveCharge(form){
    console.log(form.value);
    this.processService.saveCharge(form.value).subscribe((res) => {
    });
  }

  // get() {
  //   this.processService.getCharge(id).subscribe((data) => {
  //     console.log(data);
  //     this.charges = data;
  //   });
  // }

}
