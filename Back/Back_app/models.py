from email.policy import default

from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields import FloatField

# Create your models here.

class UserProfil(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	# image = models.ImageField(default='',name='image')
	# birthday = models.DateField()
	

class Entreprise(models.Model):
	intitule_projet = models.CharField(max_length=100)
	statut_juridique = models.CharField(max_length=100)
	n_tel = models.IntegerField()
	email = models.EmailField()
	location = models.CharField(max_length=100)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	nature = models.CharField(max_length=20)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)
	


class Financement(models.Model):
	description = models.CharField(max_length=100)
	montant = models.IntegerField()
	entreprise = models.ForeignKey(Entreprise, related_name="finances", on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)

	

class Pretbank(models.Model):
	description = models.CharField(max_length=100)
	montant = models.IntegerField()
	duree = models.IntegerField()
	taux = models.FloatField()
	entreprise = models.ForeignKey(Entreprise, related_name="prets", on_delete=models.CASCADE)

	def get_absolute_url(self):
			return "Back_app/%s/" %(self.id)

    	

class Demarrage(models.Model):
	description = models.CharField(max_length=100)
	monant  = models.IntegerField()
	entreprise = models.ForeignKey(Entreprise, related_name="demarrages", on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)

	

class Charge(models.Model):
	description = models.CharField(max_length=100)
	monant_ann1 = models.IntegerField()
	monant_ann2 = models.IntegerField()
	monant_ann3 = models.IntegerField()
	entreprise = models.ForeignKey(Entreprise, related_name="charges", on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)


class ChiffrDaffAnn(models.Model):
	description = models.CharField(max_length=100)
	montant = models.IntegerField()
	pourcentage1 = models.IntegerField()
	pourcentage2 = models.IntegerField()
	pourcentage_charge = models.IntegerField()
	entreprise = models.ForeignKey(Entreprise, related_name="chiffres", on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)
	

class ChiffrDaffMens(models.Model):
	description = models.CharField(max_length=100)
	nbre_jr = models.IntegerField()
	chiffr_jr = models.IntegerField()
	chiffr_ann = models.ForeignKey(ChiffrDaffAnn, on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)


class MyBP(models.Model):
	upload = models.FileField(upload_to='uploads/')
	user = models.ForeignKey(UserProfil, on_delete=models.CASCADE)

	def get_absolute_url(self):
    		return "Back_app/%s/" %(self.id)
