from wsgiref import validate

from cryptography.utils import read_only_property
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.gis.gdal.raster import source
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import *


class UserProfilSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfil
        fields = ('first_name', 'last_name')


class UserSerializer(serializers.ModelSerializer):
    # userprofils = UserProfilSerializer(read_only=True)
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password']
        )
        return user

    class Meta:
        model = User
        fields = ('id','username', 'email', 'password')


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user:
            return user
        raise serializers.ValidationError("cet utilisateur n'existe pas")

class UserProfilInfoSerializer(serializers.ModelSerializer):
    userprofil = UserProfilSerializer(read_only=True)
    class Meta:
        model = User
        fields = ('id', 'email', 'username','userprofil',)


class EntrepriseSerializer(serializers.ModelSerializer):
    user =serializers.PrimaryKeyRelatedField(read_only=True)

    
    class Meta:
        model= Entreprise
        fields = ('id', 'intitule_projet', 'statut_juridique', 'n_tel', 'email','location', 'nature', 'user')


class FinancementSrializer(serializers.ModelSerializer):
    # entreprise = serializers.PrimaryKeyRelatedField(read_only=True)
    
    class Meta:
        model = Financement
        fields = ('id', 'description', 'montant', 'entreprise')


class FinancesSerializer(serializers.ModelSerializer):
    finances = FinancementSrializer(many=True, read_only=True)

    class Meta:
        model = Entreprise
        fields = ('id' , 'finances',)
    

class PretbankSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Pretbank    
        fields = ('id' ,'description', 'montant', 'duree', 'taux', 'entreprise')


class PretsSerializer(serializers.ModelSerializer):
    prets = PretbankSerializer(many=True, read_only=True)

    class Meta:
        model = Entreprise
        fields = ('id' , 'prets',)


class DemarrageSerialiser(serializers.ModelSerializer):

    class Meta:
        model = Demarrage    
        fields = ('id' ,'description', 'monant', 'entreprise')


class DemarragesSerializer(serializers.ModelSerializer):
    demarrages = DemarrageSerialiser(many=True, read_only=True)

    class Meta:
        model = Entreprise
        fields = ('id','demarrages',)
        

class ChargeSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Charge    
        fields = ('id' ,'description', 'monant_ann1', 'monant_ann2', 'monant_ann3', 'entreprise')


class ChargesSerializer(serializers.ModelSerializer):
    charges = ChargeSerializer(many=True, read_only=True)

    class Meta:
        model = Entreprise
        fields = ('id','charges',)


class ChiffrDaffAnnSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChiffrDaffAnn    
        fields = ('id' ,'description', 'montant', 'pourcentage1', 'pourcentage2', 'pourcentage_charge', 'entreprise')


class ChiffresSerializer(serializers.ModelSerializer):
    chiffres = ChiffrDaffAnnSerializer(many=True, read_only=True)

    class Meta:
        model = Entreprise
        fields = ('id' , 'chiffres',)


class ChiffrDaffMensSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChiffrDaffMens    
        fields = ('id' ,'description', 'nbre_jr', 'chiffr_jr', 'chiffr_ann')

class BPserializer(serializers.ModelSerializer):

    class Meta:
        model = MyBP
        fields = '__all__'
