from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(UserProfil)
admin.site.register(Entreprise)
admin.site.register(Financement)
admin.site.register(Pretbank)
admin.site.register(Demarrage)
admin.site.register(Charge)
admin.site.register(ChiffrDaffAnn)
admin.site.register(ChiffrDaffMens)
admin.site.register(MyBP)
