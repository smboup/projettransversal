from fnmatch import filter
from os.path import defpath
from wsgiref import validate

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import request
from django.shortcuts import get_object_or_404
from knox.auth import AuthToken
from rest_framework import request, status
from rest_framework.generics import (CreateAPIView, ListAPIView,
                                     ListCreateAPIView, RetrieveAPIView)
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import *
from .serializers import *


class CreateUserView(ListCreateAPIView):
    
    model = User
    queryset =User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):

        if request.method == 'POST':
            print('test')
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            first_name =request.data['first_name']
            last_name = request.data['last_name']
            # birthday = request.data['birthday']

            userinf = serializer.save()
            UserProfil.objects.create(user=userinf, first_name=first_name, last_name=last_name)
            return Response({
                'users': UserSerializer(userinf, context=self.get_serializer_context()).data,
                "token": AuthToken.objects.create(userinf)[1]})


class LoginAPI(CreateAPIView):
    serializer_class = LoginSerializer
    queryset = User.objects.all()
    permission_classes = [
        AllowAny
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserProfil = serializer.validated_data
        return Response({
            "user": LoginSerializer(UserProfil, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(UserProfil)[1]
        })


#Recuperation de l'utilisateur pour l'afficher dans le profil
class UserProfilInfoAPIView(RetrieveAPIView):
    queryset = UserProfil.objects.all()
    serializer_class = UserProfilInfoSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

#Infomations relatives à l'enreprise pour laquelle nous créons le bussiness plan
class EntrepriseAPIView(ModelViewSet):
    queryset = Entreprise.objects.all()
    serializer_class = EntrepriseSerializer
    permission_classes = [IsAuthenticated]  
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return Entreprise.objects.filter(user=self.request.user)


class Finances(RetrieveAPIView):
    serializer_class = FinancesSerializer
    queryset = Entreprise.objects.all()
    permission_classes = [IsAuthenticated]

        
#Le financement de l'entreprise : son view
class FinanceAPIView(ModelViewSet):
    queryset = Pretbank.objects.all()
    serializer_class = FinancementSrializer
    permission_classes = [IsAuthenticated]            

#Le pretbank de l'entreprise s'il en exste
class PretbankAPIView(ModelViewSet):
    queryset = Pretbank.objects.all()
    serializer_class = PretbankSerializer 
    permission_classes = [IsAuthenticated]


class prets(RetrieveAPIView):
    serializer_class = PretsSerializer
    queryset = Entreprise.objects.all()
    permission_classes = [IsAuthenticated]

#Les financements de demarrage du projet d'entreprise
class DemarrageAPIView(ModelViewSet):
    queryset = Demarrage.objects.all()
    serializer_class = DemarrageSerialiser 
    permission_classes = [IsAuthenticated]


class Demarrages(RetrieveAPIView):
    serializer_class = DemarragesSerializer
    queryset = Entreprise.objects.all()
    permission_classes = [IsAuthenticated]

#Les charges a subir durant les trois annees qui suivent la creation de l'entreprise
class ChargeAPIView(ModelViewSet):
    queryset = Charge.objects.all()
    serializer_class = ChargeSerializer 
    permission_classes = [IsAuthenticated]


class Charges(RetrieveAPIView):
    serializer_class = ChargesSerializer
    queryset = Entreprise.objects.all()
    permission_classes = [IsAuthenticated]


#Les chiffres d'affaires annuelles
class ChiffrDaffAnnAPIView(ModelViewSet):
    queryset = ChiffrDaffAnn.objects.all()
    serializer_class = ChiffrDaffAnnSerializer 
    permission_classes = [IsAuthenticated]

    
class Chiffranns(RetrieveAPIView):
    serializer_class = ChiffresSerializer
    queryset = Entreprise.objects.all()
    permission_classes = [IsAuthenticated]

#Les chiffres d'affaire mensuelles
class ChiffrDaffMensAPIView(ModelViewSet):
    queryset = ChiffrDaffMens.objects.all()
    serializer_class = ChiffrDaffMensSerializer 
    permission_classes = [IsAuthenticated]




#etablissement du compte de resultat
def Compte_de_resultat(request):
    pass


#Etablissement du compte de bilan
def compte_de_bilan(request):
    pass

#Calcul des besoins en fond de roulement
def calcul_de_fondroulement(id_entreprise):
    finances = All_finance(id_entreprise)
    pretbanks = All_pretbank(id_entreprise)
    demarrages = All_demarrage(id_entreprise)

    fondroulement = (finances + pretbanks) - demarrages
    return fondroulement

#Verification du projet s'il est rentable
def verif_rentable(id_entreprise, pourcentage_charge, pourcentage1, pourcentage2):
    chiffres = Chiffr_daff_ann(1)
    charges = All_charge(id_entreprise)
    chiffrdaff1 = (chiffres * pourcentage1)*(1 - (pourcentage_charge/100))
    chiffrdaff2 = (chiffrdaff1 * pourcentage2)*(1 - (pourcentage_charge/100))
    return chiffrdaff2



def verif_tresorerie(request):
    pass

#Computer all financement 
def All_finance(id_entreprise):
    query = Financement.objects.all().get(entreprise=id_entreprise)
    finances = 0
    for q in query:
        finances+=q.montant
        return finances


#computer all charge
def All_charge(id_entreprise):
    query = Charge.objects.all().get(entreprise=id_entreprise)
    charges1 = 0
    charges2 = 0
    charges3 = 0
    charges = []
    for q in query:
        charges1+=q.montant1
        charges2+=q.montant2
        charges3+=q.montant3
    charges.append(charges1)
    charges.append(charges2)
    charges.append(charges3)
    return charges


#calcule de tous les chiffres d'affaires
def All_chiffr_mens(id_chiffr_ann):
    query = ChiffrDaffMens.obejcts.all().get(chiffr_ann=id_chiffr_ann)
    chiffr_mens = []
    for q in query:
        chiffr = q.nbre_jr * q.chiffr_jr
        chiffr_mens.append(chiffr)
    return chiffr_mens


def Chiffr_daff_ann(id_chiffr_ann):
    chiffres_mens = All_chiffr_mens(id_chiffr_ann)
    somme = 0
    for chifres in chiffres_mens:
        somme+=chifres
    return somme



#computer of all resources for demarrage
def All_demarrage(id_entreprise):
    query = Demarrage.objects.all().get(entreprise=id_entreprise)
    demarrages = 0
    for q in query:
        demarrages += q.montant
    return demarrages


#computer of all ressources for pretbank 
def All_pretbank(id_entreprise):
    query = Pretbank.obejects.all().get(entreprise=id_entreprise)
    somme = 0
    for q in query:
        somme+=q.montant

    return somme