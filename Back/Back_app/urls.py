from django.urls import path, include
from knox import views as knox_views

from rest_framework import routers

from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('process/entreprise', EntrepriseAPIView, base_name="createntreprise")
router.register('process/finance', FinanceAPIView, base_name='createfinance')
router.register('process/pret', PretbankAPIView, base_name='createpret')
router.register('process/demarrage', DemarrageAPIView, base_name='createdemarrage')
router.register('process/charge', ChargeAPIView, base_name='createcharge')
router.register('process/chiffrann', ChiffrDaffAnnAPIView, base_name='createchiffrann')
router.register('process/chiffrmens', ChiffrDaffMensAPIView, base_name='createchiffrmens')



urlpatterns = [
    path('register', CreateUserView.as_view(), name='userregister'),
    path('login', LoginAPI.as_view(), name='connection'),
    path('logout', knox_views.LogoutView.as_view(), name='deconnecction'),
    path('process/listfinance/<int:pk>', Finances.as_view(), name='listfinance'),
    path('process/listdemarrage/<int:pk>', Demarrages.as_view(), name='listdemarrage'),
    path('process/listcharge/<int:pk>', Charges.as_view(), name='listcharge'),
    path('process/listpret/<int:pk>', prets.as_view(), name='listpret'),
    path('process/listchiffr/<int:pk>', Chiffranns.as_view(), name='listchiffr'),
    path('process/profile', UserProfilInfoAPIView.as_view(), name='profil'),
    path('', include(router.urls)),
    # path('process/finance', FinanceAPI.as_view(), name='finance')
]

